# dot files, global scripts, runtime config files etc. 

My personal config. Use at your own risk.

store the source files here, symlink to them from where the config file is expected

## symlinks
the program loading the config file must be able to follow symlinks. For some reason, git throws a wobbly when adding symlinks to git repositories, hence storing the source files in the repository and linking to them from outside. 


syntax for symlink creation on windows 10, in admin mode cmd, from the directory in which the symlink will live (the target directory). 


`mklink name path\to\source.file`

## scripts
A folder containing shortcuts and scripts to be made accessible via the system path. 

## vim 
Vim config, including vimrc, is maintained in a separate vim-config repo.
