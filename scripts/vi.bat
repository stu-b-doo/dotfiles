# start vim in the default terminal (faster)
# %* passes the arguments (filename) to the shell script to be passed to vi
start "" "C:\Program Files\Git\usr\bin\vim.exe" %*

# To use the terminal that comes with git for windows (the default terminal for *.sh files) start a vi.sh script instead:
# vi.sh simply contains `vi "$@"`
# start "" "vi.sh" %*
