#!/bin/sh
# Open vi. 
# $@ passes the arguments (ie the filename)  from the caller to vi.
# Can call this in Windows using a batch script: `start "" "vi.sh" %*`
vi "$@"

# exec vim "$@"
