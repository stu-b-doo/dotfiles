# environment variables
export TERM=cygwin
# prompt text FIXME: git status doesn't update
# export PS1="\[\033]0;$TITLEPREFIX:${PWD//[^[:ascii:]]/?}\007\]\n\[\033[32m\]\u@\h \[\033[35m\]$MSYSTEM \[\033[33m\]\w\[\033[36m\]`__git_ps1`\[\033[0m\]\n$"
# aliases and shortcut functions
alias g='git'
alias ls='ls -1a'
alias gits='git status'
# diff color words highlights only differences between words - renders fine in MINGW64 terminal for git bash 
alias gitd='git diff -w --word-diff=plain'
alias gita='git add .'
alias gitlo='git log --oneline'
alias gitc='git commit -am'
alias exp='explorer .'
gitid() { git rev-parse HEAD~$1 | cut -c -7 | tr -d $'\n' ; }
gitl() { git log --oneline -$1 ; }
# print the nth line of input, copy to clipboard. Usage: ls | grab 3
grab () { sed -n $1p | clip ; }
# function to cd with a windows path like "C:\Users\sbell" to "/mnt/c/Users/sbell"
cdwin () { 
# Note: Since the input expects backslashes, input must be enclosed in single or double quotes - stackoverflow.com/q/35722316/
  winpath=${1//\\//}
#  winpath=${winpath/C:\///mnt/c/}
  cd "$winpath"
}

# add to PATH
# go bin 
export PATH=$PATH:"/c/Users/sbell/go/bin"

# CDPATH: can cd into any subdirectory of these paths:
CDPATH=~/ws/:~/ws/go/src/gitlab.com/stu-b-doo/

# mkdir and cd into it
mkcdir() {
	mkdir -p -- "$1" && # -p creates ancestor directories if necessary
	   cd -P -- "$1"    # -P resolves symlinks
}

# Autocomplete wildcard patterns in bash for opening files
bind "TAB:menu-complete"
bind "set show-all-if-ambiguous on"
bind "set menu-complete-display-prefix on"

# mkdir and cd https://unix.stackexchange.com/a/125386
mkcd ()
{
    mkdir -p -- "$1" &&
      cd -P -- "$1"
}
